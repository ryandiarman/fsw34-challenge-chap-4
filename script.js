const result_p = document.querySelector(".result > p");
const green_box = document.getElementById("green_box");
const refresh = document.getElementById("refreshing");
const p_rock = document.getElementById("p_rock");
const p_paper = document.getElementById("p_paper");
const p_scissors = document.getElementById("p_scissors");
const c_rock = document.getElementById("c_rock");
const c_paper = document.getElementById("c_paper");
const c_scissors = document.getElementById("c_scissors");

function getComputerChoice() {
	const choices = ['c_rock', 'c_paper', 'c_scissors'];
	const randomNumber = Math.floor(Math.random()*3);
	return choices[randomNumber];
}

function player_win() {
	// result_p.innerHTML = "PLAYER 1 WIN";
	green_box.style.display = 'block';
	green_box.innerHTML = "Player 1 Win";
	result_p.style.display = 'none';
}

function com_win() {
	// result_p.innerHTML = "COM WIN";
	green_box.style.display = "block";
	green_box.innerHTML = "COM Win";
	result_p.style.display = 'none';
}

function draw() {
	// result_p.innerHTML = "DRAW";
	green_box.style.display = "block";
	green_box.innerHTML = "DRAW";
	result_p.style.display = 'none';
}

function game(playerChoice) {
	const computerChoice = getComputerChoice();
	console.log("player => " + playerChoice);
	console.log("computer => " + computerChoice);

	if(playerChoice == 'p_rock')	{
		p_rock.style.background = 'lightgrey'
		p_paper.style.background = 'none';
		p_scissors.style.background = 'none';
	}

	if(playerChoice == 'p_paper')	{
		p_paper.style.background = 'lightgrey'
		p_rock.style.background = 'none';
		p_scissors.style.background = 'none';
	}

	if(playerChoice == 'p_scissors')	{
		p_scissors.style.background = 'lightgrey'
		p_paper.style.background = 'none';
		p_rock.style.background = 'none';
	}

	if(computerChoice == 'c_paper')	{
		c_paper.style.background = 'lightgrey'
		c_scissors.style.background = 'none';
		c_rock.style.background = 'none';
	}

	if(computerChoice == 'c_rock')	{
		c_rock.style.background = 'lightgrey'
		c_scissors.style.background = 'none';
		c_paper.style.background = 'none';
	}

	if(computerChoice == 'c_scissors')	{
		c_scissors.style.background = 'lightgrey'
		c_paper.style.background = 'none';
		c_rock.style.background = 'none';
	}

	switch (playerChoice + '=' + computerChoice) 
	{
		case "p_rock=c_scissors":
		case "p_paper=c_rock":
		case "p_scissors=c_paper":
			console.log("PLAYER 1 WINS!!!!!!");
			player_win();
			break;
		case "p_rock=c_paper":
		case "p_paper=c_scissors":
		case "p_scissors=c_rock":
			console.log("COM WIN!!!!!!");
			com_win();
			break;
		case "p_rock=c_rock":
		case "p_paper=c_paper":
		case "p_scissors=c_scissors":
			console.log("DRAW!!!!!!");
			draw();
			break;
	}
}


function main() {
	p_rock.addEventListener('click', function() {
	game('p_rock');
})
	p_paper.addEventListener('click', function() {
	game('p_paper');	
})
	p_scissors.addEventListener('click', function() {
	game('p_scissors');	
})
	refresh.addEventListener('click', function reloadThePage(){
    window.location.reload();
});	
}

main();